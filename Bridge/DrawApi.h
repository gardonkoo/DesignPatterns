//
// Created by GuoFeng on 2024/6/26.
//

#ifndef DESIGNPATTERNS_DRAWAPI_H
#define DESIGNPATTERNS_DRAWAPI_H

#include "Point.h"

class DrawApi {
public:
    virtual void draw(double x, const Point &p) = 0;
};

#endif //DESIGNPATTERNS_DRAWAPI_H
