//
// Created by GuoFeng on 2024/6/26.
//

#ifndef DESIGNPATTERNS_CONCRETEDRAWAPI_H
#define DESIGNPATTERNS_CONCRETEDRAWAPI_H

#include "DrawApi.h"
#include <iostream>

class SquareDrawApi : public DrawApi {
public:
    void draw(double x, const Point &center) override {
        std::cout << "sizeLength=" << x
                  << ", center.x=" << center.getX()
                  << ", center.y=" << center.getY()
                  << ", area=" << x * x
                  << std::endl;
    }
};

class CircleDrawApi : public DrawApi {
public:
    void draw(double x, const Point &center) override {
        std::cout << "radius=" << x
                  << ", center.x=" << center.getX()
                  << ", center.y=" << center.getY()
                  << ", area=" << 3.14 * x * x
                  << std::endl;
    }
};

#endif //DESIGNPATTERNS_CONCRETEDRAWAPI_H
