//
// Created by GuoFeng on 2024/6/26.
//
#include "ConcreteShape.h"
#include "ConcreteDrawApi.h"
#include <memory>

int main() {
    auto circle = std::make_unique<Circle>(2.5, Point(), std::make_shared<CircleDrawApi>());
    auto square = std::make_unique<Square>(3.4, Point(1.0, 2.0), std::make_shared<SquareDrawApi>());
    circle->draw();
    square->draw();
    return 0;
}
