//
// Created by GuoFeng on 2024/6/26.
//

#ifndef DESIGNPATTERNS_POINT_H
#define DESIGNPATTERNS_POINT_H

class Point {
public:
    Point() : x(0.0), y(0.0) {}

    Point(double x1, double y1) : x(x1), y(y1) {}

    double getX() const { return x; }

    double getY() const { return y; }

private:
    double x;
    double y;
};

#endif //DESIGNPATTERNS_POINT_H
