//
// Created by GuoFeng on 2024/6/26.
//

#ifndef DESIGNPATTERNS_CONCRETESHAPE_H
#define DESIGNPATTERNS_CONCRETESHAPE_H

#include "Shape.h"
#include "Point.h"
#include <memory>

class Circle : public Shape {
public:
    Circle(double radius1, const Point &center1, std::shared_ptr<DrawApi> drawApi) :
            Shape(drawApi),
            radius(radius1), center(center1) {

    }

    void draw() override {
        drawApi->draw(radius, center);
    }


private:
    double radius;
    Point center;

};

class Square : public Shape {
public:
    Square(double sideLength1, const Point &center1, std::shared_ptr<DrawApi> drawApi) :
            Shape(drawApi),
            sideLength(sideLength1),
            center(center1) {
    }

    void draw() override {
        drawApi->draw(sideLength, center);
    }

private:
    double sideLength;
    Point center;
};


#endif //DESIGNPATTERNS_CONCRETESHAPE_H
