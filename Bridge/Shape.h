//
// Created by GuoFeng on 2024/6/26.
//

#ifndef DESIGNPATTERNS_SHAPE_H
#define DESIGNPATTERNS_SHAPE_H

#include "DrawApi.h"
#include <memory>

class Shape {
public:
    Shape(std::shared_ptr<DrawApi> &drawApi1) : drawApi(drawApi1) {}

    virtual void draw() = 0;

protected:
    std::shared_ptr<DrawApi> drawApi;
};

#endif //DESIGNPATTERNS_SHAPE_H
